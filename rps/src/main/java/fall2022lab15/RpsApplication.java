package fall2022lab15;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {

    private RpsGame rps;

    public void start(Stage stage) {
        Group root = new Group();

        //scene is associated with container, dimensions
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);

        //associate scene to stage and show
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);

        VBox overall = new VBox();

        HBox buttons = new HBox();
        Button b1 = new Button("rock");
        Button b2 = new Button("paper");
        Button b3 = new Button("scissors");
        buttons.getChildren().addAll(b1,b2,b3);

        HBox textFields = new HBox();
        TextField message = new TextField("Welcome!");
        message.setPrefWidth(200);
        TextField wins = new TextField("wins : 0");
        TextField losses = new TextField("losses : 0");
        TextField ties = new TextField("ties : 0");
        textFields.getChildren().addAll(message, wins, losses, ties);
        
        overall.getChildren().addAll(buttons, textFields);
        root.getChildren().add(overall);

        this.rps = new RpsGame();

        b1.setOnAction(new RpsChoice(message, wins, losses, ties, "rock", rps));
        b2.setOnAction(new RpsChoice(message, wins, losses, ties, "paper", rps));
        b3.setOnAction(new RpsChoice(message, wins, losses, ties, "scissors", rps));

        stage.show();   
    }
    public static void main(String[] args){
        Application.launch(args);
    }
} 
