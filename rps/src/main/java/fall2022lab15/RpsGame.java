package fall2022lab15;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rng;

    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.rng = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String choice){
        int randomChoice = rng.nextInt(3);
        String cpuChoice;
        if(randomChoice == 0){
            cpuChoice = "rock";
        }
        else if(randomChoice == 1){
            cpuChoice = "paper";
        }
        else{
            cpuChoice = "scissors";
        }
        
        if((choice.equals("rock") && cpuChoice.equals("paper")) || (choice.equals("paper") && cpuChoice.equals("scissors")) || (choice.equals("scissors") && cpuChoice.equals("rock"))){
            this.losses++;
            return "Computer played " + cpuChoice + ". You lose! ";
        }
        else if ((choice.equals("rock") && cpuChoice.equals("scissors")) || (choice.equals("paper") && cpuChoice.equals("rock")) || (choice.equals("scissors") && cpuChoice.equals("paper"))){
            this.wins++;
            return "Computer played " + cpuChoice + ". You win!";
        }
        else{
            this.ties++;
            return "Computer played " + cpuChoice + ". It's a tie.";
        }
    }
}
