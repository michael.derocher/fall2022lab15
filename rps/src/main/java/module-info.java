module fall2022lab15 {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
    opens fall2022lab15 to javafx.fxml;
    exports fall2022lab15;
}